package com.cyxsssz.email.service;

/**
 * @author cyxss.cn
 * @email ride0623@qq.com
 * @date 2019/8/29 21:49
 */

public interface MailServer {
    /**
     * 发送多媒体邮件
     * @param to
     * @param subject
     * @param content
     */
    void sendMimeMail(String to, String subject, String content);
}
