package com.cyxsssz.email.controller;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author cyxss.cn
 * @email ride0623@qq.com
 * @date 2019/8/30 9:08
 */

public class IndexController {
    @RequestMapping("/")
    public String index() {
        return "index";
    }
}
