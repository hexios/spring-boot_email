package com.cyxsssz.email.dao;

import com.cyxsssz.email.entity.User;
import org.springframework.stereotype.Component;

/**
 * @author cyxss.cn
 * @email ride0623@qq.com
 * @date
 */

@Component
public interface UserDao {
    /**
     * 注册
     * @param user
     */
    void register(User user);

    /**
     * 查询激活用户
     * @param code
     * @return
     */
    User checkCode(String code);

    /**更新用户
     * @param user
     */
    void updateUserStatus(User user);

    /**
     * 登录
     * @param user
     * @return
     */
    User loginUser(User user);


}
